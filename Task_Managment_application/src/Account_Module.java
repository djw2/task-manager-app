import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.LinkedList;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;

public class Account_Module extends VBox
{
	Manager_Module mm;		//for junit test
	
	
	public Manager_Module get_new_manager_module(String name)	//for junit
	{
		return new Manager_Module(name);
	}
	
	private class User	//"struct" for users
	{
		private String username;
		private String password;

		User()
		{

		}
		User(String username, String password)
		{
			this.username = username;
			this.password = password;
		}

		public String get_password()
		{
			return password;
		}

		public String get_username()
		{
			return username;
		}

		public void set_password(String password)
		{
			this.password = password;
		}

		public void set_username(String username)
		{
			this.username = username;
		}
	}

	final File FILE;
	final boolean DEBUG;
	private LinkedList<User> user_list;

	
	Account_Module()
	{
		
		DEBUG = true;
		FILE = new File("user_data.txt");
		user_list = new LinkedList<User>();
		
		this.setPadding(new Insets(15, 15, 15, 15));
		this.setStyle("-fx-background-color: #336699;");
		this.setSpacing(11);
		this.setMinSize(500, 500);
		
		TextField usernameField = new TextField();
		PasswordField passwordField = new PasswordField();
		Button registerButton = new Button();
		Button loginButton = new Button();
		
		passwordField.setText("Enter Password");
		usernameField.setText("Enter Username");
		loginButton.setText("Login");
		registerButton.setText("Register");
		
		registerButton.setOnAction(new EventHandler<ActionEvent>()
		{
			@Override
			public void handle(ActionEvent arg0)
			{
				try
				{
				add_user_to_list(usernameField.getText(), passwordField.getText());
				} 
				catch (IOException e)
				{
					// TODO Auto-generated catch block
				e.printStackTrace();
				}

			}

		});
		
		loginButton.setOnAction(new EventHandler<ActionEvent>()
		
		{

			@Override
			public void handle(ActionEvent event)
			{
				try
				{
					if(DEBUG)System.out.println("Attempted login");
					if (attemptLogin(usernameField.getText(), passwordField.getText()))
					{
						
							
							Manager_Module hbox = new Manager_Module(usernameField.getText());
							System.out.println("LOGIN SUCCESS");
							Scene mod_scene = new Scene(hbox, 600, 600);
							Main.get_stage().setScene(mod_scene);
							
				

					} else
					{
						if (DEBUG)
							System.out.println("LOGIN FAIL");
					}
				} catch (IOException e)
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}

		});
	

		
		HBox buttonContainer = new HBox();
		buttonContainer.getChildren().addAll(registerButton, loginButton);
		buttonContainer.setSpacing(10);
		this.getChildren().addAll(usernameField, passwordField, buttonContainer);
		
		
		
		
		
	}


	public boolean add_user_to_list(String username, String password) throws IOException
	{
		User user = new User(username, password);
		refresh_user_list();
		if (user_list.isEmpty())
		{
			if (DEBUG)
				System.out.println("user list is empty");
			user_list.add(user);
		} else
		{
			if (DEBUG)
				System.out.println("user list is not empty");
			for (int i = 0; i < user_list.size(); ++i)
			{
				if (user_list.get(i).get_username().equalsIgnoreCase(user.get_username()))
				{
					if (DEBUG)
						System.out.println("user name already exists, not adding");
					
					return false;
				}
			}
			if (DEBUG)
				System.out.println("Adding user to list");
			user_list.add(user);
		}
		save_user_list();

		return true;
	}

	public boolean attemptLogin(String username, String password) throws IOException
	{
		refresh_user_list();
		for (int i = 0; i < user_list.size(); ++i)
		{
			if (user_list.get(i).get_username().equalsIgnoreCase(username))
			{
				if (user_list.get(i).get_password().equals(password))
				{
					return true;
				}

			}
		}
		return false;
	}

	private void debug_print_list()
	{
		for (int i = 0; i < user_list.size(); ++i)
		{
			System.out.println("size is " + i);
			System.out.print(user_list.get(i).get_username() + ' ');
			System.out.println(user_list.get(i).get_password());
		}
	}

	
	private void refresh_user_list() throws IOException
	{
		if (DEBUG)
			System.out.println("getting user list");

		user_list.clear();
		
		if (!FILE.exists())
		{
			user_list.clear();
			System.out.println("file wasnt found");
			return;
		} 
		else
		{
			System.out.println("file was found");
			BufferedReader bufferedReader = new BufferedReader(new FileReader(FILE));
			String line;
			line = bufferedReader.readLine();
			while (line != null)
			{
				User temp = new User();

				temp.set_username(line.substring(0, line.indexOf(',')));
				temp.set_password(line.substring(line.indexOf(',') + 1));
				if (DEBUG)
					System.out.println(temp.get_username());
				if (DEBUG)
					System.out.println(temp.get_password());
				user_list.add(temp);
				line = bufferedReader.readLine();
			}
			bufferedReader.close();
		}
	}

	private void save_user_list() throws IOException
	{
		

		BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(FILE));
		for (int i = 0; i < user_list.size(); ++i)
		{
			
			if (i != 0)
			{
				bufferedWriter.write('\n');
			}
			bufferedWriter.write(user_list.get(i).get_username());
			bufferedWriter.write(',');
			bufferedWriter.write(user_list.get(i).get_password());
		}
		bufferedWriter.close();

	}

}