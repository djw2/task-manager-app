
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.LinkedList;

import javafx.beans.Observable;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.Border;
import javafx.scene.layout.BorderStroke;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.stage.Popup;

public class Manager_Module extends HBox
{
	class Direction_Button extends Button
	{
		Direction_Button(Task_Container from, Task_Container to, String button_text, Manager_Module mm)
		{
			this.setText(button_text);
			this.setMinSize(30, 10);
			this.setOnAction(new EventHandler<ActionEvent>(){

				@Override
				public void handle(ActionEvent event)
				{
					if(!from.get_list_view().getSelectionModel().isEmpty())
					{
						int selected_index = from.get_list_view().getSelectionModel().getSelectedIndex();
						String current_task = from.get_task_list().get(selected_index);
						from.remove_task(current_task);
						to.add_task(current_task);
						from.get_list_view().getSelectionModel().clearSelection();
						to.get_list_view().getSelectionModel().clearSelection();
						try
						{
							mm.save();
						} 
						catch (IOException e)
						{
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						
					}
					
				}
				
			});
		}
	}

	class Task_Container extends VBox	//container visual and data structure for task items
	{
		
		ListView<String> task_view;
		private String title;
		ObservableList<String> task_list;
		
		Task_Container(String title)
		{
	
			task_list = FXCollections.observableArrayList();
			
			task_view = new ListView<String>();
			task_view.setOrientation(Orientation.VERTICAL);
			
			this.title = title;
			
			Label title_label = new Label();
			title_label.setText(title);
			title_label.setPrefWidth(100);
		
			
			this.getChildren().addAll(title_label, task_view);
			
			
		}

		
		public void add_task(String task)
		{
			if(DEBUG)System.out.println("Task is " +task);
			task_list.add(task);
			task_view.setItems(task_list);
			
		}
		
		public void remove_task(String task)
		{
			for(int i = 0; i < task_list.size(); ++i)
			{
				if(task_list.get(i).equals(task))
				{
					task_list.remove(i);
					i = task_list.size();
				}
			}
			task_view.setItems(task_list);

		}
		
		public ObservableList<String> get_task_list()
		{
			return task_list;
		}
		
		public ListView<String> get_list_view()
		{
			return task_view;
		}
		
		public String get_title()
		{
			return title;
		}
		
		public void set_task_list(ObservableList<String> task_list)
		{
			this.task_list = task_list;
			if(task_list != null && !task_list.isEmpty())
				task_view.setItems(task_list);

		}
		public void set_title(String title)
		{
			this.title = title;
		}

	}
	
	
	
	final boolean DEBUG;
	final File FILE;
	Task_Container todo_container;
	Task_Container doing_container;
	Task_Container done_container;
	
	

	public Manager_Module(String username)
	{
		 todo_container = new Task_Container("To Do");
		 doing_container = new Task_Container("In Progress");
		 done_container = new Task_Container("Done");
		
		DEBUG = true;
		FILE = new File(username + "_data.txt");
		
		Manager_Module mm = this;
		this.setPadding(new Insets(10,10,10,10));
		this.setBackground(new Background(new BackgroundFill(Color.TAN, CornerRadii.EMPTY, Insets.EMPTY)));
		this.setSpacing(11);
		this.setMinSize(500, 500);
		

		
		Button add_task_button = new Button();
		Button remove_task_button = new Button();
		add_task_button.setText("Add task");
		remove_task_button.setText("Remove Completed Task");
		
		
		Popup popup = new Popup();
		popup.setX(505);
		popup.setY(405);
		VBox popup_box = new VBox();
		popup_box.setLayoutX(500);
		popup_box.setLayoutY(400);
		popup_box.setMinHeight(100);
		popup_box.setMinWidth(100);
		TextField task_textfield = new TextField("Enter task here");
		Button ok_popup = new Button();
		ok_popup.setMinWidth(50);
		ok_popup.setText("ADD");
		popup_box.getChildren().addAll(task_textfield, ok_popup);
		popup.getContent().add(popup_box);
	
		
		VBox transitions1 = new VBox();
		transitions1.getChildren().add(new Direction_Button(todo_container, doing_container, "->", this));
		transitions1.getChildren().add(new Direction_Button(doing_container, todo_container, "<-", this));
		
		VBox transitions2 = new VBox();
		transitions2.getChildren().add(new Direction_Button(doing_container, done_container, "->", this));
		transitions2.getChildren().add(new Direction_Button(done_container, doing_container, "<-", this));
		
		
		
		ok_popup.setOnAction(new EventHandler<ActionEvent>()
		{

			@Override
			public void handle(ActionEvent event)
			{
				String next_task = new String();
				next_task = task_textfield.getText();
				todo_container.add_task(next_task);
				
				try
				{
					save();
				} catch (IOException e)
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				popup.hide();
			}
			
		});
		add_task_button.setOnAction(new EventHandler<ActionEvent>()
		{

			@Override
			public void handle(ActionEvent arg0)
			{
				if(DEBUG) System.out.println("Add task button click");
				popup.show(mm, mm.getLayoutX(), mm.getLayoutY());		
			}
			
		});
		remove_task_button.setOnAction(new EventHandler<ActionEvent>()
		{
			@Override
			public void handle(ActionEvent event)
				{
				if(!done_container.get_list_view().getSelectionModel().isEmpty())
				{
					int remove_index = done_container.get_list_view().getSelectionModel().getSelectedIndex();
					done_container.get_task_list().remove(remove_index);
					done_container.get_list_view().setItems(done_container.get_task_list());
					try
					{
						save();
					} 
					catch (IOException e)
					{
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				done_container.get_list_view().getSelectionModel().clearSelection();
						
						
					}
			
		});
		todo_container.getChildren().add(add_task_button);
		done_container.getChildren().add(remove_task_button);
		try
		{
			load();
		} catch (IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.getChildren().addAll(todo_container, transitions1, doing_container, transitions2, done_container);
	}
	
	private void load() throws IOException
	{
		
		if(!FILE.exists())
		{
			return;
		}
		BufferedReader bufferedReader = new BufferedReader(new FileReader(FILE));
	
		String line = bufferedReader.readLine();
		String[] task_desc = null;
		if(line != null && !line.isEmpty())
		{
			task_desc = line.split(",");
			for(int i = 0; i < task_desc.length; ++i)
			{
				todo_container.add_task(task_desc[i]);
			}
			
		}

	
		line = bufferedReader.readLine();
		task_desc = null;
		if(line != null && !line.isEmpty())
		{
			task_desc = line.split(",");

			for(int i = 0; i < task_desc.length; ++i)
			{
			
				doing_container.add_task(task_desc[i]);
			}
		
		}


		line = bufferedReader.readLine();
		task_desc = null;
		if(line != null && !line.isEmpty())
		{
			task_desc = line.split(",");
			for(int i = 0; i < task_desc.length; ++i)
			{
				done_container.add_task(task_desc[i]);
			}
			
		}

		bufferedReader.close();
		
	}
	
	private void save() throws IOException
	{
		
		BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(FILE));
		ObservableList<String> temp = todo_container.get_task_list();
		for(int i = 0; i < temp.size(); ++i)
		{
			if(i != 0)
				bufferedWriter.write(',');
			bufferedWriter.write(temp.get(i));
	
		}
		bufferedWriter.write('\n');
		temp = doing_container.get_task_list();
		for(int i = 0; i < temp.size(); ++i)
		{
			if(i != 0)
				bufferedWriter.write(',');
			bufferedWriter.write(temp.get(i));
	
		}
		bufferedWriter.write('\n');
		temp = done_container.get_task_list();
		for(int i = 0; i < temp.size(); ++i)
		{
			if(i != 0)
				bufferedWriter.write(',');
			bufferedWriter.write(temp.get(i));
	
		}
		bufferedWriter.close();
	}



		
	

}
