import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application		//basis for application
{
	private static Stage stage;
	Account_Module Test_am; 		//junit
	
	public Account_Module get_account_module()	//for junit
	{
		return Test_am;
	}
	
	public static void main(String [] args) 
	{
		launch();
	}

	@Override
	public void start(Stage stage) throws Exception
	{
		set_stage(stage);
		Account_Module account_mod = new Account_Module();
		Test_am = account_mod;
		Scene account_scene = new Scene(account_mod);
		stage.sizeToScene();
		stage.setScene(account_scene);
		stage.setTitle("Task Manager Application");
		stage.show();
		
		
		

		
	}
	
	public static Stage get_stage()
	{
		return stage;
	}
	
	public void set_stage(Stage stage)
	{
		this.stage = stage;
	}
}
