import static org.junit.Assert.*;

import java.io.IOException;

import org.junit.Test;

import junit.framework.TestCase;

import static org.junit.Assert.assertEquals;
public class Account_ModuleTest 
{

	@Test
	public void testAccount_Module() throws IOException
	{
		Main main = new Main();
		main.main(null);
		Account_Module am = main.get_account_module();
		
		boolean valid_register = am.add_user_to_list("a_new_name", "anything_goes");
		assertEquals(valid_register, true);
		boolean invalid_register = am.add_user_to_list("a_new_name", "anything_goes");
		assertEquals(invalid_register, false);
		

	}

}
